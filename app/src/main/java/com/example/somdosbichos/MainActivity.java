package com.example.somdosbichos;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageButton caoButton;
    private ImageButton gatoButton;
    private ImageButton leaoButton;
    private ImageButton macacoButton;
    private ImageButton ovelhaButton;
    private ImageButton vacaButton;

    private MediaPlayer mediaPlayer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        caoButton = findViewById(R.id.CaoID);
        gatoButton = findViewById(R.id.GatoID);
        leaoButton = findViewById(R.id.LeaoID);
        macacoButton = findViewById(R.id.MacacoID);
        ovelhaButton = findViewById(R.id.OvelhaID);
        vacaButton = findViewById(R.id.VacaID);


        caoButton.setOnClickListener(this);
        gatoButton.setOnClickListener(this);
        leaoButton.setOnClickListener(this);
        macacoButton.setOnClickListener(this);
        ovelhaButton.setOnClickListener(this);
        vacaButton.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.CaoID:
                mediaPlayer = MediaPlayer.create(MainActivity.this, R.raw.cao);
                TocarSom();
                break;
        }
        switch (v.getId()){
            case R.id.GatoID:
                mediaPlayer = MediaPlayer.create(MainActivity.this, R.raw.gato);
                TocarSom();
                break;

        }
        switch (v.getId()){
            case R.id.LeaoID:
                mediaPlayer = MediaPlayer.create(MainActivity.this, R.raw.leao);
                TocarSom();
                break;

        }
        switch (v.getId()){
            case R.id.MacacoID:
                mediaPlayer = MediaPlayer.create(MainActivity.this , R.raw.macaco);
                TocarSom();
                break;
        }
        switch ((v.getId())){
            case R.id.OvelhaID:
                mediaPlayer = MediaPlayer.create(MainActivity.this, R.raw.ovelha);
                TocarSom();
                break;
        }
        switch (v.getId()){
            case R.id.VacaID:
                mediaPlayer = MediaPlayer.create(MainActivity.this, R.raw.vaca);
                TocarSom();
                break;
        }

        }




    private void TocarSom() {
        if (mediaPlayer != null) {
            mediaPlayer.start();
        }

    }
    @Override
    protected void onDestroy() {


        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;

        }
        super.onDestroy();
        Toast.makeText(MainActivity.this,"Metodo onDestroy chamado", Toast.LENGTH_SHORT).show();
    }
}

/*
public class MainActivity extends AppCompatActivity {

    private ImageButton caoButton;
    private MediaPlayer caoMidia;

    private ImageButton gatoButton;
    private MediaPlayer gatoMidia;

    private ImageButton leaoButton;
    private MediaPlayer leaoMidia;

    private ImageButton macacoButton;
    private MediaPlayer macacoMidia;

    private ImageButton ovelhaButton;
    private MediaPlayer ovelhaMidia;

    private ImageButton vacaButton;
    private MediaPlayer vacaMidia;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        caoButton = findViewById(R.id.CaoID);
        caoMidia = MediaPlayer.create(MainActivity.this,R.raw.cao);

        gatoButton = findViewById(R.id.GatoID);
        gatoMidia = MediaPlayer.create(MainActivity.this, R.raw.gato);

        leaoButton = findViewById(R.id.LeaoID);
        leaoMidia = MediaPlayer.create(MainActivity.this, R.raw.leao);

        macacoButton = findViewById(R.id.MacacoID);
        macacoMidia = MediaPlayer.create(MainActivity.this,R.raw.macaco);

        ovelhaButton = findViewById(R.id.OvelhaID);
        ovelhaMidia = MediaPlayer.create(MainActivity.this, R.raw.ovelha);

        vacaButton = findViewById(R.id.VacaID);
        vacaMidia = MediaPlayer.create(MainActivity.this,R.raw.vaca);



        caoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SomCao();

            }
        });

        gatoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
Somgato();
            }
        });

        leaoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
Somleao();
            }
        });

        macacoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
SomMacaco();
            }
        });

        ovelhaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
SomOvelha();
            }
        });

        vacaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
SomVaca();
            }
        });

    }

    private void SomCao(){
        caoMidia.start();

    }
    private void Somgato(){
        gatoMidia.start();
    }
    private void Somleao(){
        leaoMidia.start();
    }
    private void SomMacaco(){
        macacoMidia.start();
    }
    private void SomOvelha(){
        ovelhaMidia.start();
    }
    private void SomVaca(){
        vacaMidia.start();
    }

    @Override
    protected void onDestroy() {

        if(caoMidia != null && caoMidia.isPlaying()){
            caoMidia.stop();
           caoMidia.release();
           caoMidia = null;

        }
        else if(gatoMidia != null & gatoMidia.isPlaying()) {
            gatoMidia.stop();
            gatoMidia.release();
            gatoMidia = null;
        }

        else if(leaoMidia != null && leaoMidia.isPlaying()){
            leaoMidia.stop();
            leaoMidia.release();
            leaoMidia = null;
        }
        else if(macacoMidia != null && macacoMidia.isPlaying()){
            macacoMidia.stop();
            macacoMidia.release();
            macacoMidia = null;
        }
        else if(ovelhaMidia != null &&  ovelhaMidia.isPlaying()){
            ovelhaMidia.stop();
            ovelhaMidia.release();
            ovelhaMidia = null;
        }

       else if(vacaMidia != null && vacaMidia.isPlaying()){
            vacaMidia.stop();
            vacaMidia.release();
            vacaMidia = null;
        }

        super.onDestroy();
        Toast.makeText(MainActivity.this,"Metodo onDestroy chamado", Toast.LENGTH_SHORT).show();
    }

}
 */